use serde::*;

#[derive(Clone, Copy, Eq, PartialEq, PartialOrd, Ord, Debug, Serialize, Deserialize)]
pub struct Card(pub i32);

impl Card {
    pub fn bulls(&self) -> i32 {
        match self.0 {
            55 => 7,
            x if x % 11 == 0 => 5,
            x if x % 10 == 0 => 3,
            x if x % 5 == 0 => 2,
            _ => 1,
        }
    }

    pub fn create_deck() -> Vec<Card> {
        (1..=104).map(|c| Card(c)).collect::<Vec<_>>()
    }
}

#[cfg(test)]
mod card_tests {
    use super::*;

    #[test]
    fn test_serialize_card() {
        let str = serde_json::to_string_pretty(&Card(42)).unwrap();
        assert_eq!(str, "42")
    }
}
