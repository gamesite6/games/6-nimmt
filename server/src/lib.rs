use rand::prelude::*;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};

mod game;
use game::*;

pub type PlayerId = i32;

#[derive(Clone, Debug, Deserialize, Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Settings {
    pub player_counts: HashSet<usize>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GameState {
    players: Vec<PlayerState>,
    rows: [Vec<Card>; 4],
    deck: Vec<Card>,
    phase: Phase,

    /**
        The number of points collected by each player per round.
    */
    summary: Vec<HashMap<PlayerId, i32>>,
}

impl GameState {
    fn player_mut(&mut self, player_id: PlayerId) -> Option<&mut PlayerState> {
        self.players.iter_mut().find(|p| p.id == player_id)
    }

    fn all_cards_placed(&self) -> bool {
        self.players.iter().all(|p| p.played.is_none())
    }
    fn all_hands_empty(&self) -> bool {
        self.players.iter().all(|p| p.hand.is_empty())
    }
    fn bull_limit_reached(&self) -> bool {
        self.players.iter().any(|p| p.score >= 66)
    }

    fn prepare_new_round<R>(&mut self, rng: &mut R)
    where
        R: Rng,
    {
        let mut deck = Card::create_deck();
        deck.shuffle(rng);

        let round_summary = self
            .players
            .iter()
            .map(|p| {
                let points: i32 = p.taken.iter().map(Card::bulls).sum();
                (p.id, points)
            })
            .collect::<HashMap<PlayerId, i32>>();

        self.summary.push(round_summary);

        for player in self.players.iter_mut() {
            player.taken.clear();
            player.hand = deck.split_off(deck.len() - 10);
            player.played = None;
        }

        self.rows = [
            deck.pop().into_iter().collect::<Vec<_>>(),
            deck.pop().into_iter().collect::<Vec<_>>(),
            deck.pop().into_iter().collect::<Vec<_>>(),
            deck.pop().into_iter().collect::<Vec<_>>(),
        ];

        self.phase = Phase::Playing
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PlayerState {
    id: PlayerId,
    hand: Vec<Card>,
    played: Option<Card>,
    taken: Vec<Card>,
    score: i32,
}

impl PlayerState {
    fn take_cards(&mut self, cards: &mut Vec<Card>) {
        self.score += cards.iter().map(|c| Card::bulls(c)).sum::<i32>();
        self.taken.append(cards)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, Eq, PartialEq)]
#[serde(tag = "k", content = "v")]
pub enum Phase {
    Playing,
    Placing,
    Taking { actor: PlayerId },
    RoundComplete { ready: HashSet<PlayerId> },
    GameComplete,
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(tag = "k", content = "v")]
pub enum Action {
    Play { card: Card },
    Take { row: usize },
    Ready,
}

pub fn valid_player_counts() -> [usize; 9] {
    [2, 3, 4, 5, 6, 7, 8, 9, 10]
}

pub fn initial_state(
    players_ids: &[PlayerId],
    settings: &Settings,
    seed: i64,
) -> Option<GameState> {
    if !settings.player_counts.contains(&players_ids.len()) {
        return None;
    }

    let mut rng = rand_pcg::Pcg32::from_seed(i128::from(seed).to_le_bytes());

    let mut deck = Card::create_deck();
    deck.shuffle(&mut rng);

    let players = players_ids
        .iter()
        .map(|&id| {
            let hand = deck.split_off(deck.len() - 10);
            PlayerState {
                id,
                hand,
                played: None,
                taken: vec![],
                score: 0,
            }
        })
        .collect::<Vec<_>>();

    let rows = [
        deck.pop().into_iter().collect::<Vec<_>>(),
        deck.pop().into_iter().collect::<Vec<_>>(),
        deck.pop().into_iter().collect::<Vec<_>>(),
        deck.pop().into_iter().collect::<Vec<_>>(),
    ];

    Some(GameState {
        players,
        deck,
        rows,
        phase: Phase::Playing,
        summary: vec![],
    })
}

pub fn perform_action(
    previous_state: &GameState,
    action: &Action,
    _settings: &Settings,
    actor_id: PlayerId,
    seed: i64,
) -> Option<GameState> {
    let actor: &PlayerState = previous_state.players.iter().find(|p| p.id == actor_id)?;

    if !action_is_allowed(action, previous_state, actor)? {
        return None;
    }

    let mut state = previous_state.clone();

    match &previous_state.phase {
        Phase::Playing { .. } => match action {
            Action::Play { card } => {
                let actor_mut = state.player_mut(actor.id)?;
                let (card_hand_idx, _card) =
                    actor.hand.iter().enumerate().find(|(_idx, c)| *c == card)?;

                actor_mut.hand.remove(card_hand_idx);
                actor_mut.played.replace(*card);

                if state.players.iter().all(|p| p.played.is_some()) {
                    state.phase = Phase::Placing;
                }
            }

            _ => return None,
        },
        Phase::Placing => match action {
            Action::Ready => {
                let actor_mut = state.player_mut(actor_id)?;
                let player_card = actor_mut.played.take()?;
                let row = if state.rows.iter().any(|r| r.is_empty()) {
                    state.rows.iter_mut().find(|r| r.is_empty())?
                } else {
                    state
                        .rows
                        .iter_mut()
                        .filter_map(|r| {
                            if *r.last()? < player_card {
                                Some(r)
                            } else {
                                None
                            }
                        })
                        .max_by(|x, y| x.last().cmp(&y.last()))?
                };

                row.push(player_card);

                if row.len() > 5 {
                    state.phase = Phase::Taking { actor: actor_id };
                } else {
                    state.phase = match (
                        state.all_cards_placed(),
                        state.all_hands_empty(),
                        state.bull_limit_reached(),
                    ) {
                        (true, true, true) => Phase::GameComplete,
                        (true, true, false) => Phase::RoundComplete {
                            ready: HashSet::new(),
                        },
                        (true, false, _) => Phase::Playing,
                        (false, _, _) => Phase::Placing,
                    };
                }
            }
            Action::Take { row: row_index } => {
                let row = state.rows.get_mut(*row_index)?;
                let mut cards = row.split_off(0);
                let actor_mut = state.player_mut(actor_id)?;
                actor_mut.take_cards(&mut cards);
            }
            _ => return None,
        },
        Phase::Taking { .. } => match action {
            &Action::Take { row } => {
                let row = state.rows.get_mut(row)?;

                let last_card = row.pop()?;
                let mut taken_cards = vec![];
                taken_cards.append(row);

                *row = vec![last_card];

                let actor_mut = state.player_mut(actor_id)?;
                actor_mut.take_cards(&mut taken_cards);

                state.phase = match (
                    state.all_cards_placed(),
                    state.all_hands_empty(),
                    state.bull_limit_reached(),
                ) {
                    (true, true, true) => Phase::GameComplete,
                    (true, true, false) => Phase::RoundComplete {
                        ready: HashSet::new(),
                    },
                    (true, false, _) => Phase::Playing,
                    (false, _, _) => Phase::Placing,
                };
            }
            _ => return None,
        },
        Phase::RoundComplete { .. } => match action {
            Action::Ready => {
                if let Phase::RoundComplete { ready } = &mut state.phase {
                    ready.insert(actor_id);
                }

                if let Phase::RoundComplete { ready } = &state.phase {
                    if state.players.iter().all(|p| ready.contains(&p.id)) {
                        let mut rng = rand_pcg::Pcg32::from_seed(i128::from(seed).to_le_bytes());
                        state.prepare_new_round(&mut rng)
                    }
                }
            }
            _ => return None,
        },
        Phase::GameComplete => return None,
    }

    Some(state)
}

fn action_is_allowed(action: &Action, state: &GameState, actor: &PlayerState) -> Option<bool> {
    let result = match &state.phase {
        Phase::Playing => match action {
            Action::Play { card } => actor.played.is_none() && actor.hand.contains(&card),
            _ => false,
        },
        Phase::Placing => {
            let lowest_played = state.players.iter().filter_map(|p| p.played).min()?;
            let actor_played = actor.played?;

            match action {
                Action::Ready => {
                    lowest_played == actor_played
                        && state.rows.iter().any(|r| match r.iter().max() {
                            Some(&c) => c < actor_played && r.len() < 6,
                            None => true,
                        })
                }
                &Action::Take { row } => {
                    let row_maxes = state
                        .rows
                        .iter()
                        .filter_map(|r| r.iter().max())
                        .copied()
                        .collect::<Vec<_>>();

                    lowest_played == actor_played // actor currently has the lowest played card
                        && row < 4 // taken row index is valid
                        && state.rows.iter().all(|r| !r.is_empty()) // no row is empty
                        && row_maxes.iter().all(|&c| c > actor_played) // all rows have higher value than actor's played card
                }
                _ => false,
            }
        }
        &Phase::Taking { actor: actor_id } => match action {
            &Action::Take { row } => state.rows.get(row)?.len() > 5 && actor_id == actor.id,
            _ => false,
        },
        Phase::RoundComplete { ready } => match action {
            Action::Ready => !ready.contains(&actor.id),
            _ => false,
        },
        Phase::GameComplete => false,
    };
    Some(result)
}

pub fn is_game_complete(state: &GameState) -> bool {
    match state.phase {
        Phase::GameComplete => true,
        _ => false,
    }
}
