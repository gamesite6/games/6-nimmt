export function getDefaultSettings() {
  return {
    playerCounts: [4, 5, 6, 7, 8, 9, 10],
  };
}

export const validPlayerCounts = [2, 3, 4, 5, 6, 7, 8, 9, 10];

export function playerCounts(settings: GameSettings): number[] {
  return validPlayerCounts.filter((c) => settings.playerCounts.includes(c));
}
