/// <reference types="svelte" />
/// <reference types="vite/client" />

type PlayerId = number;

type Card = number;
type GameState = {
  players: PlayerState[];
  deck: Card[];
  rows: [Card[], Card[], Card[], Card[]];
  phase: Phase;
  summary: Array<{ [playerId: string]: number }>;
};

type PlayerState = {
  id: PlayerId;
  hand: Card[];
  played: Card | null;
  taken: Card[];
  score: number;
};

type GameSettings = { playerCounts: number[] };

type Action =
  | { k: "Play"; v: { card: Card } }
  | { k: "Ready" }
  | { k: "Take"; v: { row: number } };

type Phase =
  | { k: "Playing" }
  | { k: "Placing" }
  | { k: "Taking"; v: { actor: PlayerId } }
  | { k: "RoundComplete"; v: { ready: PlayerId[] } }
  | { k: "GameComplete" };
