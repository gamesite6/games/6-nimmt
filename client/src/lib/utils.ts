export function range(from: number, to: number): number[] {
  const size = Math.max(to - from, 0);
  const result = new Array(size);

  for (let i = 0; i < size; i++) {
    result[i] = from + i;
  }

  return result;
}

export function rangeInclusive(from: number, to: number): number[] {
  return range(from, to + 1);
}

export function sorted<T extends number>(ts: T[]): T[] {
  let result = [...ts];
  result.sort((a, b) => a - b);
  return result;
}

export function sortedByScore(players: PlayerState[]): PlayerState[] {
  let result = [...players];
  result.sort((a, b) => a.score - b.score);
  return result;
}

export function getPlayerOrder(
  players: PlayerState[],
  userId: PlayerId | null
): PlayerId[] {
  let playerIds = players.map((p) => p.id);
  let userIndex = userId !== null ? playerIds.indexOf(userId) : -1;

  if (userIndex === -1) {
    return playerIds;
  } else {
    return [...playerIds.slice(userIndex), ...playerIds.slice(0, userIndex)];
  }
}
