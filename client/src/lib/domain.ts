export function getActivePlayers(state: GameState): PlayerId[] {
  switch (state.phase.k) {
    case "Playing":
      return state.players.filter((p) => p.played === null).map((p) => p.id);

    case "Placing":
      const lowestCard = Math.min(
        ...state.players
          .map((p) => p.played as number)
          .filter((c) => c !== null)
      );
      return state.players
        .filter((p) => p.played === lowestCard)
        .map((p) => p.id);

    case "Taking":
      return [state.phase.v.actor];
    case "RoundComplete":
      let { ready } = state.phase.v;
      return state.players.map((p) => p.id).filter((p) => !ready.includes(p));
    case "GameComplete":
      return [];
  }
}

export function getRowMaxes(rows: Card[][]): Card[] {
  return rows.map((r) => Math.max(...r));
}

export function getTakableRows(state: GameState, playerId: PlayerId): number[] {
  switch (state.phase.k) {
    case "Placing": {
      let playerCard =
        state.players.find((p) => p.id === playerId)?.played ?? null;
      if (playerCard !== null) {
        return getRowMaxes(state.rows).every((c) => c > (playerCard as number))
          ? [0, 1, 2, 3]
          : [];
      } else {
        return [];
      }
    }

    case "Taking":
      let rowIndex = state.rows.findIndex((r) => r.length > 5);
      return rowIndex !== -1 ? [rowIndex] : [];
    default:
      return [];
  }
}

export function playerCanPlaceCard(
  state: GameState,
  playerId: PlayerId
): boolean {
  switch (state.phase.k) {
    case "Placing":
      let card = state.players.find((p) => p.id === playerId)?.played ?? null;
      if (card !== null) {
        return getRowMaxes(state.rows).some((c) => c < (card as number));
      } else {
        return false;
      }

    default:
      return false;
  }
}

export function getBulls(card: Card): number {
  if (card === 55) {
    return 7;
  } else if (card % 11 === 0) {
    return 5;
  } else if (card % 10 === 0) {
    return 3;
  } else if (card % 5 === 0) {
    return 2;
  } else {
    return 1;
  }
}
