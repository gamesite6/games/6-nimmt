import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/bullheaded/",
  build: {
    emptyOutDir: false,
    lib: {
      entry: "src/index.ts",
      fileName: "bullheaded",
      name: "Gamesite6_Bullheaded",
    },
  },
  plugins: [svelte()],
});
